# EUFS Mobile Robotics Crash Course

This repository contains supporting material for the Mobile Robotics Crash Course organised by [Edinburgh University Formula Student (EUFS)](http://eufs.co).
The aim of the Crash Course is to provide a hands-on introduction to the main aspects of mobile robotics as related to the work done at EUFS for the Formula Student AI competition. 

The crash course involves 4 modules:
1. Introduction to ROS
2. Perception
3. Sensor Fusion for State Estimation
4. Planning & Control

We also include instructions on setting up your development environment (e.g ROS) in [0. Environment Setup](0.\ Environment\ Setup).
